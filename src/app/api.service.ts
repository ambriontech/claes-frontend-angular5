import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpRequest,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class ApiService {
  public selectedNavMenu:string;
  private baseurl = 'http://127.0.0.1/claes-api/';
  constructor(private http: HttpClient) { }
  apigetData(apiurl): Observable<any> {
      return this.http.get(this.baseurl+apiurl);
      
   }

   apiPostData(apiurl,data) {
     return this.http.post(this.baseurl+apiurl,data);
   }

   apiPutData(apiurl,data) {
    return this.http.put(this.baseurl+apiurl,data);
  }

  apiDelete(apiurl){
    return this.http.delete(this.baseurl+apiurl);
  }

   onSelectNav(menuItem:string){
    this.selectedNavMenu=menuItem;
  }

}
