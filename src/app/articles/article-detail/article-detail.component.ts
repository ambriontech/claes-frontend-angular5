import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../api.service';
import {articleGroupModel} from '../article_group.model';
import {NgForm} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { articleModel } from '../article.model';
import { DialogService } from "ng2-bootstrap-modal";
declare var $:any;
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})

export class ArticleDetailComponent implements OnInit {
  private articleGroup:articleGroupModel[]=[];
  defaultImage:string='assets/images/default.png';
  private articleDetail=new articleModel(0,'',0,0,0,0,0,'1970-2-12',0,0,0,0,'',this.defaultImage);
  private unitsArr=[];
  private vatLevels=[];
  private resultUnits=[];
  @ViewChild('fileInput') fileInput:ElementRef;
  articlePop:articleModel[]=[];
  articles:articleModel[]=[];
  date:Date=new Date();
    settings = {
        bigBanner: false,
        timePicker: true,
        format: 'yyyy-MM-dd',
        defaultOpen: false
    }

  constructor(private apiService:ApiService,private router: Router,private proute: ActivatedRoute,
    private dialogService:DialogService,private alertService: AlertService) { }

  ngOnInit() {
    this.getArticleGroupListing('article-groups');
    this.getSettings('settings');
    this.proute.params
      .subscribe( 
        params => {
          const articleId=params.id;
          if(articleId>0){
            this.getArticleDetail('articles/'+articleId);
            this.getPackages('get-package/'+articleId);
          }
        }
      
      );
    
  }

  //get article detail with service
  getArticleDetail(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       const articleLength=Object.keys(res).length;
       if(articleLength>0) {
        res.image=(res.image)?(res.image):this.defaultImage;
        if(res.date){
          this.date= (res.date=='')?new Date():new Date(res.date);
        }
        
          this.articleDetail=new articleModel(res.id,res.name,res.price,res.article_group,res.unit,
            res.result_unit,res.vat_level,res.date,res.layout1,res.layout2,res.layout3,res.bookable,
            res.info,res.image);
            
        }
     });
   }
  
  //get article group listing 
  getArticleGroupListing(url){
    this.apiService.apigetData(url)
    .subscribe(res => {
      const articleLength=Object.keys(res).length;
      if(articleLength>0) {
        for (let prop in res) {
          this.articleGroup.push(new articleGroupModel(res[prop].id,res[prop].name,res[prop].parent_id));
          }
        
        }
    });
  }
  
  //get setting from db
  getSettings(url){
    this.apiService.apigetData(url)
    .subscribe(res => {
      const resultLength=Object.keys(res).length;
        if(resultLength>0) {
          for(let prop in res){
            switch(res[prop].name){
              case 'Units':
              this.unitsArr = JSON.parse(res[prop].value);
              break;
              case 'VAT Levels':
              this.vatLevels = JSON.parse(res[prop].value);
              break;
              case 'Result Units':
              this.resultUnits=JSON.parse(res[prop].value);
              break;
            }
          }
        }
    });
  }

  onSubmit(ev,addFormValues) {
    ev.preventDefault();
    const articleId=this.articleDetail.id;
    const date=new Date(addFormValues.date);
    let month=date.getMonth()+1;
    addFormValues.date=date.getFullYear()+'-'+month+'-'+date.getDate();
    this.apiService.apiPutData('articles/'+articleId,addFormValues)
    .subscribe(
      res => {
        this.alertService.success('Artikeln är sparad');
        this.router.navigateByUrl('article');
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  onFileChange() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      let formData = new FormData();
      formData.append('image',fileBrowser.files[0]);
      
      this.apiService.apiPostData('updateImage/'+this.articleDetail.id+'/article',formData)
      .subscribe(
        res => {
         this.articleDetail.image=(res['image']!='')?res['image']:this.defaultImage;
        },
        err => {
          console.log("Error occured");
        }
      );
    }
  }

  deleteArticle(ev){
    ev.preventDefault();
    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort artikeln?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('articles/'+this.articleDetail.id)
          .subscribe(
            res => {
              this.alertService.success('Artikeln är borttagen');
              this.router.navigateByUrl('article');
            },
            err => {
              console.log("Error occured");
            }
          );
        }
        else {
          disposable.unsubscribe();
        }
    });
    
  }

  savePackageArticles(formValues){
    formValues.article_id=this.articleDetail.id;
    this.apiService.apiPostData('package-articles',formValues)
    .subscribe(res => {
      let articleLength=Object.keys(res).length;
      if(articleLength>0) {
        this.getPackages('get-package/'+this.articleDetail.id);
        $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('hide');
      }
    });
  }

  getPackages(url){
    this.apiService.apigetData(url)
    .subscribe(res => {
      const articleLength=Object.keys(res).length;
      if(articleLength>0) {
        this.articles=[];
        for (let prop in res) {
          var groupname=(res[prop].group)?res[prop].group.name:'';
          this.articles.push(new articleModel(res[prop].id,res[prop].name,res[prop].price,groupname,
            res[prop].unit,res[prop].result_unit,res[prop].vat_level,res[prop].date,res[prop].layout1,
            res[prop].layout2,res[prop].layout3,res[prop].bookable,res[prop].info,res[prop].image,res[prop].package_id));
          }
        
        }
    });
  }


 getArticleDropdown(url){
  this.apiService.apigetData(url+'/'+this.articleDetail.id)
    .subscribe(res => {
      let articleLength=Object.keys(res).length;
      if(articleLength>0) {
        this.articlePop=[];
        for (let prop in res) {
          var groupname=(res[prop].group)?res[prop].group.name:'';
          this.articlePop.push(new articleModel(res[prop].id,res[prop].name,res[prop].price,groupname,
            res[prop].unit,res[prop].result_unit,res[prop].vat_level,res[prop].date,res[prop].layout1,
            res[prop].layout2,res[prop].layout3,res[prop].bookable,res[prop].info,res[prop].image,res[prop].package_id));
          }
        
        }
    });
 }

  deletePackage(package_id,index,ev){
    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort artikeln?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('package-articles/'+package_id)
          .subscribe(
            res => {
              this.articles.splice(index,1);
              this.alertService.success('Artikeln är borttagen');
            },
            err => {
              this.alertService.danger("Error occured");
            }
          );
        }
        else {
          disposable.unsubscribe();
        }
    });
}

  openAddPackagePop(ev){
    ev.preventDefault();
    this.getArticleDropdown('article-package');
    $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('show');
  }

  print(ev){
    ev.preventDefault();
    window.print();
  }

  closeModal(){
    $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('hide');
  }
}
