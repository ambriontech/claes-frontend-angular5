import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesComponent } from './articles/articles.component';
import { HttpClientModule } from '@angular/common/http';
import { NgSemanticModule } from 'ng-semantic';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { FormsModule }   from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { DeleteDialogComponent } from '../shared/delete-dialog/delete-dialog.component';
import { PaginationComponent } from '../shared/pagination/pagination.component';

/**
 * Routing
 */
const appRoutes: Routes = [
  { path: 'article', component: ArticlesComponent },
  { path: 'article-detail', component: ArticleDetailComponent },
  { path: 'article-detail/:id', component: ArticleDetailComponent },
];

@NgModule({
  imports: [
    CommonModule,
    NgSemanticModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    SharedModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    AngularDateTimePickerModule,
    BootstrapModalModule.forRoot({container:document.body})
  ],
  entryComponents: [
    DeleteDialogComponent
  ],
  declarations: [ArticlesComponent, ArticleDetailComponent,DeleteDialogComponent,PaginationComponent],
  exports: [ArticlesComponent,ArticleDetailComponent,DeleteDialogComponent,PaginationComponent]
})
export class ArticlesModule { }
