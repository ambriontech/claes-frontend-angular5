import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router,ActivatedRoute } from '@angular/router';
import { cafeteriaModel } from '../cafeteria.model';
import { SMModalTagsDirective, SemanticModalComponent } from 'ng-semantic';
declare var $: any;
import { articleModel } from '../../articles/article.model';
import { cafeteriaArticleModel } from '../cafeteria-article.model';
import { AlertService } from 'ngx-alerts';
import { DialogService } from "ng2-bootstrap-modal";
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-cafeteria-detail',
  templateUrl: './cafeteria-detail.component.html',
  styleUrls: ['./cafeteria-detail.component.css']
})
export class CafeteriaDetailComponent implements OnInit {

  constructor(private apiService:ApiService,private proute: ActivatedRoute,
              private router: Router,private alertService: AlertService,private dialogService:DialogService) { }
  cafeteriaId:number;
  private cafeteriaDetail=new cafeteriaModel(0,0,0,'',0,'',0,0,0,'','','','','','','','');
  customerArr=[];
  usersArr=[];
  articles:articleModel[]=[];
  cafeteriaArticles:cafeteriaArticleModel[]=[];
  cafeteriaArticleTotal:number=0;
  cafeteriaArticleVat:number=0;
  cafeteriaArticleTotalVat:number=0;
  articlePopId:number=0;
  articlePopQuantity:number=0;
  MackeriaPopId:number=0;
  deliveryDate: Date = new Date();
  settings = {
    bigBanner: false,
    timePicker: false,
    format: 'yyyy-MM-dd',
    defaultOpen: false
  }

  //oninit method
  ngOnInit() {
    this.getCustomers('customers');
    this.getUsers('users');
    this.proute.params
      .subscribe( 
        params => {
          const cafeteriId=params.id;
          if(cafeteriId>0){
            this.getCafeteriaDetail('mackerias/'+cafeteriId);
            this.getCafeteriaArticles('article-mackeria/'+cafeteriId);
          }
        }
      );
  }

  //get cafeteria detail with service
  getCafeteriaDetail(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       const cafeteriaLength=Object.keys(res).length;
       if(cafeteriaLength>0) {
        if(new Date(res.delivery_date).toString()!='Invalid Date'){
          this.deliveryDate= (res.delivery_date=='')?new Date():new Date(res.delivery_date);
        }
         res.deliveryStatus=(res.delivered)?'Ja':'Nej';
          this.cafeteriaDetail=new cafeteriaModel(res.id,res.customer_id,res.contact_id,
            res.delivery_date,res.delivered,res.info,res.total,res.vat_total,res.total_incl_vat,
            res.deleted_at,res.created_at,res.updated_at,res.customer_name,res.contact_name,
            res.contact_email,res.contact_moble,res.deliveryStatus);
            this.selectedContact(res.contact_id,res.contact_email,res.contact_moble);
         }
     });
   }
   
   //get all customers
   getCustomers(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       if(Object.keys(res).length>0) {
        for(let prop in res){
          this.customerArr.push(res[prop]);
        }
        }
     });
   }

   //get all users
   getUsers(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       if(Object.keys(res).length>0) {
         for(let prop in res){
            this.usersArr.push(res[prop]);
         }
        }
     });
   }

   //selctcontact change method
   selectedContact(value,email='',mob=''){
    let usrArr=[];
    let userDetail
     if(email=='' && mob==''){
      usrArr=this.usersArr.filter(
        item =>  item.id=== parseInt(value));
        userDetail=usrArr[0];
     }
     this.cafeteriaDetail.contactId=value;
     this.cafeteriaDetail.userEmail=(email)?email:userDetail.email;
     this.cafeteriaDetail.userMobile=(mob)?mob:userDetail.phone;
   }

   //onsubmit section
   onSubmit(addFormValues) {
    const cafeteriaId=this.cafeteriaDetail.id;
    const date=new Date(addFormValues.delivery_date);
    let month=date.getMonth()+1;
    addFormValues.delivery_date=date.getFullYear()+'-'+month+'-'+date.getDate();
    this.apiService.apiPutData('mackerias/'+cafeteriaId,addFormValues)
    .subscribe(
      res => {
        console.log(res);
        this.alertService.success('Beställningen är sparad');
        this.router.navigateByUrl('cafeteria');
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  //open article pop
  openAddArticlePop(ev){
    ev.preventDefault();
    this.getGroupArticles();
    $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('show');
  }

  //get group articles
  getGroupArticles(){
    this.articles=[];
    let formData=new FormData();
    formData.append('field','article_group');
    formData.append('value','5');

      //call api service
      this.apiService.apiPostData('filterBy',formData)
      .subscribe(res => {
        let articleLength=Object.keys(res).length;
        if(articleLength>0) {
          for (let prop in res) {
            var groupname=(res[prop].group)?res[prop].group.name:'';
            this.articles.push(new articleModel(res[prop].id,res[prop].name,res[prop].price,groupname,res[prop].unit,res[prop].result_unit,res[prop].vat_level,res[prop].date,res[prop].layout1,res[prop].layout2,res[prop].layout3,res[prop].bookable,res[prop].info,res[prop].image));
          }
        }
      });
  }

  //save cafeteria article
  saveCafeteriaArticles(articleAddPop){
    articleAddPop.mackeria_id=this.cafeteriaDetail.id;
    //call api service
    this.apiService.apiPostData('mackeria-articles',articleAddPop)
    .subscribe(res => {
      let articleLength=Object.keys(res).length;
      if(articleLength>0) {
        /*this.cafeteriaArticles.push(new cafeteriaArticleModel(res['id'],res['mackeria_id'],
          res['article_id'],res['date'],res['quantity'],res['name'],res['price'],
          res['total'],res['vat_total'],res['total_incl_vat'],res['deleted_at'],
          res['created_at'],res['updated_at']));
        this.cafeteriaArticleTotal+=parseInt(res['total']);
        this.calculateTotal(this.cafeteriaArticleTotal);*/
        this.alertService.success('Mackeria artikel successfully saved');
        this.getCafeteriaArticles('article-mackeria/'+this.cafeteriaDetail.id);
        $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('hide');
      }
    });
  }

  //get cafeteria articles 
  getCafeteriaArticles(url){
   this.cafeteriaArticles.length=0;
    this.apiService.apigetData(url)
     .subscribe(res => {
       const cafeteriaLength=Object.keys(res).length;
       if(cafeteriaLength>0) {
            this.cafeteriaArticleTotal=0;
            this.articlePopId=0;
            this.articlePopQuantity=0;
            this.MackeriaPopId=0;

            let cafeteriaArticleLength=Object.keys(res).length;
            if(cafeteriaArticleLength>0){
              for(let prop in res){
                this.cafeteriaArticles.push(new cafeteriaArticleModel(res[prop].id,res[prop].mackeria_id,
                  res[prop].article_id,res[prop].date,res[prop].quantity,res[prop].name,res[prop].price,
                  res[prop].total,res[prop].vat_total,res[prop].total_incl_vat,res[prop].deleted_at,
                  res[prop].created_at,res[prop].updated_at));
                this.cafeteriaArticleTotal+=parseInt(res[prop].total);
                this.calculateTotal(this.cafeteriaArticleTotal);
                
              }
              
            }
            
         }
     });
  }

  //delete article
  deleteArticle(article,index,ev){
    ev.preventDefault();
    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort artikeln?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('mackeria-articles/'+article.id)
          .subscribe(res => {
            this.alertService.success('Artikeln är borttagen');
          this.cafeteriaArticles.splice(index,1);
          this.cafeteriaArticleTotal=this.cafeteriaArticleTotal-article.total;
          this.calculateTotal(this.cafeteriaArticleTotal);
        });
        }
        else {
          disposable.unsubscribe();
        }
    });
  }

  //calculate total
  calculateTotal(total){
    this.cafeteriaArticleVat=Math.round(0.25*total);
    this.cafeteriaArticleTotalVat=Math.round(total+this.cafeteriaArticleVat);
  }

  //delete cafeteria button
  deleteCafeteria(ev){
    ev.preventDefault();

    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort beställningen?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('mackerias/'+this.cafeteriaDetail.id)
          .subscribe(
            res => {
              this.router.navigateByUrl('cafeteria');
            },
            err => {
              console.log("Error occured");
            }
      );
        }
        else {
          disposable.unsubscribe();
        }
    });

  }

  //on print functionality
  onPrint(ev){
    ev.preventDefault();
    window.print();
  }

  editArticlePop(ev,article){
    if(ev.target.className!='far fa-trash-alt custom-fa-trash-alt'){
      this.getGroupArticles();
      this.articlePopId=article.articleId;
      this.articlePopQuantity=article.quantity;
      this.MackeriaPopId=article.id;
      $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('show');
    }
       
  }

  closeModal(){
    $('#articlePop').modal('setting', 'transition', 'vertical flip').modal('hide');
  }
}
