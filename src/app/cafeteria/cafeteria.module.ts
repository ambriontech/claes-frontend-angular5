import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSemanticModule } from 'ng-semantic';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CafeteriaListingComponent } from './cafeteria-listing/cafeteria-listing.component';
import { CafeteriaDetailComponent } from './cafeteria-detail/cafeteria-detail.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

/**
 * Routing
 */
const appRoutes: Routes = [
  { path: 'cafeteria', component: CafeteriaListingComponent },
  { path: 'cafeteria-detail/:id', component: CafeteriaDetailComponent }
];

@NgModule({
  imports: [
    CommonModule,
    NgSemanticModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    SharedModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    AngularDateTimePickerModule,
    BootstrapModalModule.forRoot({container:document.body})
  ],
  declarations: [CafeteriaListingComponent, CafeteriaDetailComponent],
  exports: [CafeteriaListingComponent]
})
export class CafeteriaModule { }
