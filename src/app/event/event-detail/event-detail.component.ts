import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { eventModel } from '../event.model';
import { ApiService } from '../../api.service';
import {Router,ActivatedRoute} from "@angular/router";
import { ParticipantModel } from '../participant.model';
import { AlertService } from 'ngx-alerts';
import { DialogService } from "ng2-bootstrap-modal";
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {
  defaultImage:string='assets/images/default.png';
  private eventDetail:eventModel=new eventModel(0,'','','','','',0,0,'',this.defaultImage,'');
  private eventParticipants:ParticipantModel[]=[];
  @ViewChild('fileInput') fileInput:ElementRef;
  @ViewChild('eventParticipant') eventParticipant:ElementRef;
  eventDate:Date=new Date();
  timeFrom:Date=new Date();
  timeTo:Date=new Date();
  settings = {
    bigBanner: false,
    timePicker: false,
    format: 'yyyy-MM-dd',
    defaultOpen: false
  }
  timeSettings = {
    bigBanner: true,
    timePicker: true,
    format: 'H:m',
    defaultOpen: false
  }

  constructor(private proute: ActivatedRoute,private apiService:ApiService,
    private router: Router,private alertService: AlertService,
    private dialogService:DialogService) { }
  
  ngOnInit() {
    this.proute.params
      .subscribe( 
        params => {
          const eventId=params.id;
          if(eventId>0){
            this.getEventDetail('events/'+eventId);
          }
        }
      
      );
  }

  //get event detail with API
  getEventDetail(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       const eventLength=Object.keys(res).length;
       if(eventLength>0) {
          res.image=(res.image)?(res.image):this.defaultImage;
          let participantArr=res.participant;
          for(let prop in participantArr){
            this.eventParticipants.push(new ParticipantModel(participantArr[prop].id,
              participantArr[prop].date,participantArr[prop].name,participantArr[prop].company,
              participantArr[prop].mobile,participantArr[prop].email,participantArr[prop].eventId,
              participantArr[prop].createdAt,participantArr[prop].updatedAt));
          }
          if(res.date){
            this.eventDate=new Date(res.date);
          }
          if(res.time_to){
            this.timeTo=new Date(res.time_to);
          }
          if(res.time_from){
            this.timeFrom=new Date(res.time_from);
          }
          this.eventDetail=new eventModel(res.id,res.date,res.name,res.location,res.time_from,
            res.time_to,res.max_persons,res.price,res.info,res.image,res.created_at);
         }
     });
   
  }

  //submit form with template driven
  onSubmit(ev,addFormValues) {
    const eventId=this.eventDetail.id;
    const date=new Date(addFormValues.date);
    let month=date.getMonth()+1;
    addFormValues.date=date.getFullYear()+'-'+month+'-'+date.getDate();
    const timeto=new Date(addFormValues.time_to);
    const timetoHours=timeto.getHours();
    addFormValues.time_to=addFormValues.date+' '+timetoHours+':'+timeto.getMinutes()+':'+timeto.getSeconds();

    const timefrom=new Date(addFormValues.time_from);
    const timefromHours=timefrom.getHours();
    addFormValues.time_from=addFormValues.date+' '+timefromHours+':'+timefrom.getMinutes()+':'+timefrom.getSeconds();
    
      this.apiService.apiPutData('events/'+eventId,addFormValues)
      .subscribe(
      res => {
        this.alertService.success('Eventet är sparat');
        this.router.navigateByUrl('events');
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  //upload file with API call
  onFileChange() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      let formData = new FormData();
      formData.append('image',fileBrowser.files[0]);
      
      this.apiService.apiPostData('updateImage/'+this.eventDetail.id+'/event',formData)
      .subscribe(
        res => {
         this.eventDetail.image=(res['image']!='')?res['image']:'assets/images/default.png';
        },
        err => {
          console.log("Error occured");
        }
      );
    }
  }

  //delete participants with API
  deleteParticipant(participantId,index){
    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort deltagaren?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('deleteParticipant/'+this.eventDetail.id+'/'+participantId)
          .subscribe(
          res => {
            this.eventParticipants.splice(index,1);
            this.alertService.success('Deltagaren har tagits bort');
          },
          err => {
            console.log("Error occured");
          }
      );
        }
        else {
          disposable.unsubscribe();
        }
    });
  }

  //delete event
  deleteEvent(ev){
    ev.preventDefault();
    let disposable = this.dialogService.addDialog(DeleteDialogComponent, {
      title:'Vill du verkligen ta bort eventet?', 
      message:'Confirm message'})
      .subscribe((isConfirmed)=>{
        //We get dialog result
        if(isConfirmed) {
          this.apiService.apiDelete('events/'+this.eventDetail.id)
          .subscribe(
              res => {
                this.alertService.success('Eventet är borttaget');
                this.router.navigateByUrl('events');
              },
              err => {
                console.log("Error occured");
              }
          );
        }
        else {
          disposable.unsubscribe();
        }
    });
  }

  //print full page
  onPrint(ev){
    ev.preventDefault();
    window.print();
  }

  //print participant
  onPrintParticipant(ev,printSectionId: string){
    ev.preventDefault();
    let popupWinindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link href="http://localhost:4200/assets/css/print.css" type="text/css" rel="stylesheet"/> <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css" /></head><body onload="window.print()"><div class="ui container custom-container custom-container-layout"><div class="ui grid"><div class="six wide column">' + innerContents + '</div></div></div></html>');
        popupWinindow.document.close();
  }
}
