import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { eventModel } from '../event.model';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-event-listing',
  templateUrl: './event-listing.component.html',
  styleUrls: ['./event-listing.component.css']
})
export class EventListingComponent implements OnInit {
  events:eventModel[]=[];
  p: number = 1;
  constructor(private apiService:ApiService,private router: Router) { }

  ngOnInit() {
    this.getEventListing('events');
  }

  //get event listing with API
  getEventListing(url){
    this.apiService.apigetData(url)
     .subscribe(res => {
       const eventLength=Object.keys(res).length;
       if(eventLength>0) {
         for (let prop in res) {
           this.events.push(new eventModel(res[prop].id,res[prop].date,res[prop].name,
            res[prop].location,res[prop].time_from,res[prop].time_to,res[prop].max_persons,
            res[prop].price,res[prop].info,res[prop].image,res[prop].created_at,res[prop].participants));
           }
         
         }
     });
   
    }

    //edit event
    editEvent(event:eventModel){
      console.log(event.id);
      this.router.navigateByUrl('event-detail/'+event.id);
    }

    //event detail with API call
    eventDetail(url){
      let formData=new FormData();
      this.apiService.apiPostData(url,formData)
     .subscribe(res => {
       const eventLength=Object.keys(res).length;
       if(eventLength>0) { 
            this.router.navigateByUrl('event-detail/'+res['id']);
         }
     });
    }

}
