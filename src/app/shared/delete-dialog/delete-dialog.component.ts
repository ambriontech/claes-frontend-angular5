import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup, FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent  extends DialogComponent<any, boolean> {

 

  constructor(dialogService: DialogService) {
    super(dialogService); 
  } 

  ngOnInit() { 
    this.result = false;     
  }

  ConfirmDelete() {
    this.result = true;   
    this.close(); 
         
  }  


}
 