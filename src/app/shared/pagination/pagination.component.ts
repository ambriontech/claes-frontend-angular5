import * as _ from 'lodash'
import { Component, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common'
import { GlobalVariable } from './globals';
import { Subject } from 'rxjs/Subject'
@Component({
  selector: 'pagination', 
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
  encapsulation: ViewEncapsulation.None, 
})
export class PaginationComponent {

  totalPage: number = 0

  @Input()
  params: {[key: string]: string | number} = {}

  @Input()
  total: number = 0

  @Input() 
  page: number = 1

  @Input()
  path: string = ""

  @Output()
  goTo: EventEmitter<number> = new EventEmitter<number>()

 

  constructor(protected _location: Location) {}

  totalPages() {
    //# 10 items per page per default
    return Math.ceil(this.total / GlobalVariable.ITEM_PAGE)
  }

  rangeStart() {
    return Math.floor(this.page / GlobalVariable.ITEM_PAGE) * GlobalVariable.ITEM_PAGE + 1
  }

  pagesRange() {
    return _.range(this.rangeStart(), Math.min(this.rangeStart() + GlobalVariable.ITEM_PAGE , this.totalPages() + 1))
  }


  prevPage() {
    return Math.max(this.rangeStart(), this.page - 1)
  }

  nextPage() {
    return Math.min(this.page + 1, this.totalPages()) 
  }

  pageParams(page: number) {
    let params =this.params
    params['page'] = page
    return params
  }

  pageClicked(page: number) {
    //# this is not ideal but it works for me
    /*const instruction = this._router.generate([
      this._router.root.currentInstruction.component.routeName,
      this.pageParams(page)
    ])
    */
    //var instruction = this.path;
    //# We change the history of the browser in case a user press refresh
    //this._location.go('/'+instruction.toLinkUrl())
    //this._location.go('/quotes/list')
    this.goTo.next(page)
  }

  checkPageAddClass(page, p) {
    if(page == p) {
      return true;
    }
    return false
  }
 
 
}
