import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HeaderComponent, MobileHeaderComponent],
  exports: [HeaderComponent,MobileHeaderComponent]
})
export class SharedModule { }
